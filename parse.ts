import { DOMParser, Element } from "https://deno.land/x/deno_dom@v0.1.12-alpha/deno-dom-wasm.ts";
import { writeAllSync, readAllSync } from "https://deno.land/std@0.99.0/io/util.ts";

const html = new TextDecoder().decode(readAllSync(Deno.stdin));
const document = new DOMParser().parseFromString(html, 'text/html')!;

// Remove one anomalous extra p element
document.querySelectorAll('h3 + p + p').forEach(p => p.remove());

const listElements = <Element[]>[...document.querySelectorAll('h3 + p + ul')];

const codeRegex = /width=(?<width>\d+) poly=(?<poly>0x.+?) init=(?<init>0x.+?) refin=(?<refin>.+?) refout=(?<refout>.+?) xorout=(?<xorout>0x.+?) check=(?<check>0x.+?) residue=(?<residue>0x.+?) name=\"(?<name>.+)\"/;

const enum Format { bin = '0b', hex = '0x', unknown = '' }

const formatOf = (text: string) => {
  if (/^[01]+$/.test(text)) return Format.bin;
  if (/^[\da-f]+$/i.test(text)) return Format.hex;
  return Format.unknown;
};

const parseAlgorithm = (ul: Element) => {
  const codeText = ul.previousElementSibling!.textContent.replace(/\s+/g, ' ').trim();
  const data = codeText.match(codeRegex)!.groups!;
  const codeElements = [...ul.querySelectorAll('ul ul ul code')];
  const codewords = codeElements
    .map(c => c.textContent.replace(/\W/g, '').trim())
    .filter(c => formatOf(c) !== Format.unknown)
    .map(c => formatOf(c) + c);
  return {
    ...data,
    width: +data.width,
    refin: data.refin === 'true',
    refout: data.refout === 'true',
    codewords,
  };
};

const algorithms = listElements.map(parseAlgorithm);

const json = JSON.stringify(algorithms, null, 2);
writeAllSync(Deno.stdout, new TextEncoder().encode(json));
