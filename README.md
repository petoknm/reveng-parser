# reveng-parser

Parses RevEng CRC Catalogue and extracts algorithm data into `models.json`.

## Dependencies

 - deno

## How to use it

The script expects HTML on its stdin and outputs JSON on stdout. This allows us to use it a variety of contexts, like reading from a local HTML file, saving the output to a local file or just viewing it, etc.

```bash
$ curl http://reveng.sourceforge.net/crc-catalogue/all.htm | deno run parse.ts > models.json
```
